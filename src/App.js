import React, { Component } from 'react';
import "./App.css";
import ListItem from "./components/ListItem";
import ListAsset from "./components/ListAsset";
import _ from "lodash";

import "bootstrap/dist/css/bootstrap.min.css";
import Dropdown from 'react-bootstrap/Dropdown';

import { collections, assets } from "./data";

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      collections,
      assets,
      idSelected: null,
      masterAssetId: null
    }
  }

  handleOnClick = (id, masterAssetId) => {
    this.setState({
      idSelected: id,
      masterAssetId
    })
  }

  handleOnClickAsset = id => {
    const { collections } = this.state;
    collections.forEach(collection => {
      if (collection.id == this.state.idSelected) {
        collection.masterAssetId = id
      }
    })
    this.setState({
      collections,
      masterAssetId: id
    })
  }

  handleOnClickSortId = () => {
    const { assets } = this.state;
    let array = assets;
    array.sort((a, b) => a.id - b.id)
    console.log(array);
    this.setState({
      assets: array
    })
  }

  handleOnClickSortName = () => {
    const { assets } = this.state;
    let array = assets;
    array.sort((a, b) => a.name.localeCompare(b.name))
    console.log(array);
    this.setState({
      assets: array
    })
  }

  render() {
    const { collections, assets } = this.state;
    return (
      <div className="container">
        <div className="collections">
          <h1>Collections</h1>
          <ul>
            {collections.map(collection => (
              <ListItem
                key={collection.id}
                id={collection.id}
                masterAssetId={collection.masterAssetId}
                image={assets.find(a => a.id === collection.masterAssetId).path}
                name={collection.name}
                handleOnClick={this.handleOnClick}
              />
            ))}
          </ul>
        </div>
        <div className="assets">
          <h1>Assets</h1>
          <ul>
            {assets.map(asset =>
              asset.collectionId == this.state.idSelected ? (
                <ListAsset
                  key={asset.id}
                  image={asset.path}
                  name={asset.name}
                  id={asset.id}
                  idMaster={this.state.masterAssetId}
                  collectionId={asset.collectionId}
                  handleOnClickAsset={this.handleOnClickAsset}
                />
              ) : null
            )}
          </ul>
        </div>
        <div className="filter">
          <Dropdown>
            <Dropdown.Toggle variant="success" id="dropdown-basic">
              Sort Assets
            </Dropdown.Toggle>
            <Dropdown.Menu>
              <Dropdown.Item onClick={() => this.handleOnClickSortId()}>Sort by Id</Dropdown.Item>
              <Dropdown.Item onClick={() => this.handleOnClickSortName()}>Sort by Name</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </div>
      </div>
    );
  }
}

export default App;
