import React, { Component } from "react";
import Button from 'react-bootstrap/Button';
import StarsIcon from '@material-ui/icons/Stars';

const styles = {
    wrapper: {
        display: "flex",
        flexDirection: "row",
        justifyContent: 'space-between',
        alignItems: "center",
        padding: "1em"
    },
    name: {
        fontSize: "1.5em",
        marginRight: "10px"  
    },
    button: {
        width: "120px"
    }
};

const AssetBody = ({ image, name, id, idMaster, handleOnClickAsset }) => (
    <div style={styles.wrapper}>
        <img src={require(`../../images/${image}`)} width="100" />
        <span style={styles.name}>{id}</span>
        <span style={styles.name}>{name}</span>
        {id != idMaster ? (
            <Button style={styles.button} onClick={() => handleOnClickAsset(id)}>Make MasterAsset</Button>
        ) : <StarsIcon/>}
    </div>
)

export default AssetBody;