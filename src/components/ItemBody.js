import React, { Component } from "react";

const styles = {
    wrapper: {
        display: "flex",
        flexDirection: "row",
        justifyContent: 'space-between',
        alignItems: "center",
        padding: "1em"
    },
    image: {
        fontSize: "2em"
    },
    name: {
        fontSize: "1.5em"
    }
};

const ItemBody = ({ image, name }) => (
    <div style={styles.wrapper}>
        <img src={require(`../../images/${image}`)} width="100"/>
        <span style={styles.name}>{name}</span>
    </div>
)

export default ItemBody;