import React, { Component } from "react";
import ListBody from "./ItemBody";
import AssetBody from "./AssetBody";

const styles = {
    li: {
        display: "flex",
        justifyContent: "flex-start",
        background: "white",
        boxShadow: "2px 4px 10px rgba(0, 0, 0, 0.2)",
        color: "#707070",
        marginBottom: "1em",
        cursor: "pointer"
    },
    leftWall: color => ({
        width: "0.5em",
        backgroundColor: color
    })
}

class ListAsset extends Component {
    render() {
        return (
            <li style={styles.li}>
                <AssetBody id={this.props.id} idMaster={this.props.idMaster} name={this.props.name}
                    image={this.props.image} collectionId={this.props.collectionId}
                    handleOnClickAsset={this.props.handleOnClickAsset} />
            </li>
        )
    }
}

export default ListAsset;
