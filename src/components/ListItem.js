import React, { Component } from "react";
import ItemBody from "./ItemBody";
import AssetBody from "./AssetBody";

const styles = {
    li: {
        display: "flex",
        justifyContent: "flex-start",
        background: "white",
        boxShadow: "2px 4px 10px rgba(0, 0, 0, 0.2)",
        color: "#707070",
        marginBottom: "1em",
        cursor: "pointer"
    },
    leftWall: color => ({
        width: "0.5em",
        backgroundColor: color
    })
}

class ListItem extends Component {
    render() {
        return (
                <li style={styles.li} onClick={() => this.props.handleOnClick(this.props.id, this.props.masterAssetId)}>
                    <ItemBody id={this.props.id} name={this.props.name} image={this.props.image} />
                </li>
        )
    }
}

export default ListItem;